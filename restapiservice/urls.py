from django.contrib import admin
from django.urls import path
from rest_framework import routers
from django.conf.urls import include
from . import views
from rest_framework_simplejwt.views import(
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView,
)

router=routers.DefaultRouter()
router.register('users',views.UserViewSet)

urlpatterns = [
    path('token/',TokenObtainPairView.as_view() ,name='token_obtain_pair'),
    path('token/refresh',TokenRefreshView.as_view(),name='token_refresh'),
    path('token/verify',TokenVerifyView.as_view(),name='token_verify'),
    path('register/',views.CreateUser.as_view() ,name='register'),
    path('account/', include(router.urls)),
]