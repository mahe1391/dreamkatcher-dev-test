from django.contrib.auth.models import BaseUserManager


class MyUserManager(BaseUserManager):

    def create_user(self, email, password=None,first_name=None,last_name=None,phone=None,address=None):
        if not email:
            raise ValueError('Users must have an email address')

        if not password:
            raise ValueError('Users must have a valid password')

        user_obj = self.model(
            email=self.normalize_email(email)
        )

        user_obj.set_password(password)
        user_obj.first_name= first_name
        user_obj.last_name =last_name
        user_obj.phone = phone
        user_obj.address = address
        user_obj.save(using=self._db)
        return user_obj


