from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from .appusermanager import MyUserManager
from django.contrib.auth.models import PermissionsMixin


class UserAccount(AbstractBaseUser,PermissionsMixin):
    first_name = models.CharField(max_length=70)
    last_name = models.CharField(max_length=70)
    email = models.CharField(verbose_name="email",max_length=60,unique=True)
    phone = models.CharField(max_length=15,blank=True)
    address = models.CharField(max_length=15, blank=True)

    USERNAME_FIELD = 'email'

    REQUIRED_FIELDS = ['first_name', 'last_name','phone','address']

    objects = MyUserManager()

    class Meta:
        db_table = 'UserAccount'
