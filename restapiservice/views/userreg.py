from rest_framework import viewsets, status
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework import generics

from ..models import UserAccount
from ..serializers import UserSerializer


class CreateUser(generics.CreateAPIView):
    queryset = UserAccount.objects.all()
    serializer_class = UserSerializer
    permission_classes = (AllowAny,)


class UserViewSet(viewsets.ModelViewSet):
    queryset = UserAccount.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)
