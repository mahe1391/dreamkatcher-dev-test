from rest_framework import serializers
from django.contrib.auth.hashers import make_password
from ..models import UserAccount


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model=UserAccount
        fields=['email','password','first_name', 'last_name','phone','address']
        extra_kwargs={ 'password': {'write_only':True, 'required':True}}

    def create(self, validated_data):
        user = UserAccount.objects.create_user(**validated_data)
        return user
