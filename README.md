# DreamKatcher Dev Test

1. Install Python (used version 3.8.2)  and PostgreSQL 12  
2. Create a virtual environment
3. Install the below libraries inside the virtual environment
  
   * Pip install django
  
   * pip install djangorestframework
  
   * pip install djangorestframework_simplejwt
  
   * pip install psycopg2

3. EndPoints
  
   * User Registration
     http://127.0.0.1:8000/api/register/  
   * Token Auth
     http://127.0.0.1:8000/api/token/
  
   * User Detail
     http://localhost:8000/api/account/users/


